/*	Renegade Scripts.dll
	Copyright 2013 Tiberian Technologies

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#pragma once

#include "gmplugin.h"

class Polls : public Plugin
{
public:
	struct Configuration
	{
		bool Enabled;

		float PollTimer;
		float SkipThreshold;
		float SkipDisabledTimer;

		bool PollSkip;
		bool PollGameover;
		bool PollSetNext;

		void SetDefaults()
		{
			Enabled = true;

			PollTimer = 120.0f;
			SkipThreshold = 51.0f;
			SkipDisabledTimer = 120.0f;

			PollSkip = true;
			PollGameover = true;
			PollSetNext = true;
		}
	};
	
	Configuration global;
	Configuration map;

	Polls();
	~Polls();
	
	void OnLoadGlobalINISettings(INIClass *ini);
	void OnLoadMapINISettings(INIClass *ini);
	void OnObjectCreate(void *data, GameObject *o);
	void OnPlayerLeave(int ID);
	void OnLoadLevel();
	void OnGameOver();
	bool OnChat(int PlayerID, TextMessageEnum Type, const wchar_t *Message, int recieverID);

	GameObject *Get_Controller()
	{
		return Commands->Find_Object(m_iController);
	}

private:
	int m_iController;

	void LoadGenericINISettings(INIClass *ini, const char *section, Configuration &config, const Configuration &def)
	{
		config.Enabled = ini->Get_Bool(section, "PollsEnabled", def.Enabled);

		config.PollGameover = ini->Get_Bool(section, "PollGameover", def.PollGameover);
		config.PollSetNext = ini->Get_Bool(section, "PollSetNext", def.PollSetNext);
		config.PollSkip = ini->Get_Bool(section, "PollSkip", def.PollSkip);

		config.PollTimer = ini->Get_Float(section, "PollTimer", def.PollTimer);
		config.SkipThreshold = ini->Get_Float(section, "SkipThreshold", def.SkipThreshold);
		config.SkipDisabledTimer = ini->Get_Float(section, "SkipDisabledTimer", def.SkipDisabledTimer);
	}
};

extern Polls plugin;
