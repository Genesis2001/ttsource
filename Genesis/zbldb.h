#pragma once

class Database
{
	static sqlite3 *db;
	static int Callback(void *Data, int argc, char **ColumnData, char **Columns);

	static void DeleteList(void *stack);
	static void CreateTables();

public:
	struct Column
	{
		char *Name;
		char *Data;
	};

	struct Row
	{
		Stacker<Column *> *Columns;
	};

	static void Startup(StringClass dbfile);
	static void Shutdown();

	static const char *Query(Stacker<Row *> *result, const char *Format, ...);
	static void DeleteResult(Stacker<Row *> *result);
	static char *GetColumnData(const char *name, Stacker<Column *> *row);
};
