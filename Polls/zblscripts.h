#pragma once
#pragma warning (disable: 4005)

#define _HAS_EXCEPTIONS 0

#include <map>

enum
{
	POLL_TIMER_LOOP = 550,
	
	POLL_CUSTOM_START = 560,
	POLL_CUSTOM_STOP,
	POLL_CUSTOM_VOTE,
	POLL_CUSTOM_PLAYER_LEAVE,

	POLL_TYPE_SKIP = 570,
	POLL_TYPE_SETNEXT,
	POLL_TYPE_GAMEOVER,

	POLL_PARAM_NOPLAYERS = 580,
	POLL_PARAM_VOTE_SKIP,
	POLL_PARAM_VOTE_YES = 581,
	POLL_PARAM_VOTE_SKIP_CANCEL,
	POLL_PARAM_VOTE_NO = 582
};

class zbl_Poll_Controller : public ScriptImpClass
{
	int m_iLoopCount;
	int m_iPollType;
	bool m_bAcceptVotes;

	int m_iYesVotes;
	int m_iNoVotes;

	//DynamicVectorClass<int> m_lPlayers;
	std::map<int, int> m_lPlayers;

	void AddVote(int ID, int vote);
	const char *GetPollType();
	bool HasVoted(int ID);

public:
	void Created(GameObject *o);
	void Custom(GameObject *o, int type, int param, GameObject *sender);
	void Timer_Expired(GameObject *o, int num);
};

void Vote(int ID, int vote);
void SkipVote(int ID);
