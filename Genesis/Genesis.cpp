/*	Renegade Scripts.dll
	Copyright 2013 Tiberian Technologies

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#include "General.h"
#include "Genesis.h"
#include "engine_tt.h"
#include "engine_io.h"
#include "gmgame.h"

Genesis::Genesis()
{
	RegisterEvent(EVENT_LOAD_LEVEL_HOOK, this);
	RegisterEvent(EVENT_GLOBAL_INI, this);
	RegisterEvent(EVENT_MAP_INI, this);
	RegisterEvent(EVENT_PLAYER_JOIN_HOOK,this);
	RegisterEvent(EVENT_PLAYER_LEAVE_HOOK,this);
	RegisterEvent(EVENT_OBJECT_CREATE_HOOK, this);

	PlayerClass::Startup();
}
Genesis::~Genesis()
{
	Database::Shutdown();

	UnregisterEvent(EVENT_OBJECT_CREATE_HOOK, this);
	UnregisterEvent(EVENT_PLAYER_LEAVE_HOOK,this);
	UnregisterEvent(EVENT_PLAYER_JOIN_HOOK,this);
	UnregisterEvent(EVENT_MAP_INI, this);
	UnregisterEvent(EVENT_GLOBAL_INI, this);
	UnregisterEvent(EVENT_LOAD_LEVEL_HOOK, this);
}

void Genesis::LoadGenericINISettings(INIClass *ini, const char *section, Configuration& config, const Configuration &def)
{
	config.GameMode = ini->Get_Int(section, "GameMode", def.GameMode);
}

// Events
void Genesis::OnLoadLevel()
{
	const char *mapName = The_Game()->Get_Map_Name();
	Settings::MapLoad(mapName);
}
void Genesis::OnLoadGlobalINISettings(INIClass *ini)
{
	LoadGenericINISettings(ini, "Genesis", globalConfig);

	// Load database.
	ini->Get_String(globalConfig.database, "sqlite", "Database", "Genesis.db");

	Database::Startup(globalConfig.database);
}
void Genesis::OnLoadMapINISettings(INIClass *ini)
{
	const StringClass mapName = StringClass(The_Game()->Get_Map_Name());

	LoadGenericINISettings(ini, mapName, mapConfig, globalConfig);
}
void Genesis::OnObjectCreate(void *data, GameObject *o)
{
	if (o->As_SoldierGameObj() && Get_Player_ID(o))
	{
		if (mapConfig.GameMode == 2)
		{
			//Attach_Script_Once(o, "zbl_Coop_Player", "");
		}
	}
}
void Genesis::OnPlayerJoin(int PlayerID, const char *PlayerName)
{
	PlayerClass::Joined(PlayerID);
}
void Genesis::OnPlayerLeave(int PlayerID)
{
	PlayerClass::Leave(PlayerID);
}
// /Events

Genesis plugin;

extern "C" __declspec(dllexport) Plugin* Plugin_Init()
{
	return &plugin;
}
