/*	Renegade Scripts.dll
	Copyright 2013 Tiberian Technologies

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#include "General.h"
#include "Skip.h"
#include "engine_tt.h"
#include "engine_io.h"
#include "gmgame.h"

//SkipVoteController::m_bSkipStarted = false;
//SkipVoteController::m_iSkipCount = 0;

void PPage(int ID, const char *Msg, ...)
{
	char buffer[256];
	va_list va;
	_crt_va_start(va, Msg);
	vsnprintf(buffer, 256, Msg, va);
	va_end(va);

	GameObject *plr = Get_GameObj(ID);
	if (plr)
	{
		Send_Message_Player(plr, 15, 246, 96, buffer);
		Create_2D_Sound_Player(plr, "yo1.wav");
	}
}

void PPageErr(int ID, const char *Msg, ...)
{
	char buffer[256];
	va_list va;
	_crt_va_start(va, Msg);
	vsnprintf(buffer, 256, Msg, va);
	va_end(va);

	GameObject *plr = Get_GameObj(ID);
	if (plr)
	{
		Send_Message_Player(plr, 255, 107, 107, buffer);
		Create_2D_Sound_Player(plr, "yo1.wav");
	}
}

void Skip_Cast_Vote(GameObject *plr)
{
	if (!plr) return;

	int ID = Get_Player_ID(plr);
	SkipVoteController::CastVote(ID);
}

void Skip_Delete_Vote(GameObject *plr)
{
	if (!plr) return;
	
	int ID = Get_Player_ID(plr);
	SkipVoteController::DeleteVote(ID);
}


Skip::Skip()
{
	RegisterEvent(EVENT_GLOBAL_INI, this);
	RegisterEvent(EVENT_MAP_INI, this);
	RegisterEvent(EVENT_CHAT_HOOK, this);
	RegisterEvent(EVENT_PLAYER_LEAVE_HOOK, this);
	RegisterEvent(EVENT_LOAD_LEVEL_HOOK, this);
	RegisterEvent(EVENT_GAME_OVER_HOOK, this);
}

Skip::~Skip()
{
	UnregisterEvent(EVENT_GLOBAL_INI, this);
	UnregisterEvent(EVENT_MAP_INI, this);
	UnregisterEvent(EVENT_CHAT_HOOK, this);
	UnregisterEvent(EVENT_PLAYER_LEAVE_HOOK, this);
	UnregisterEvent(EVENT_LOAD_LEVEL_HOOK, this);
	UnregisterEvent(EVENT_GAME_OVER_HOOK, this);
}

void Skip::OnLoadGlobalINISettings(INIClass *ini)
{
	global.SetDefaults();
	LoadGenericINISettings(ini, "Skip", global, global);
}

void Skip::OnLoadMapINISettings(INIClass *ini)
{
	StringClass mapName = StringClass(The_Game()->Get_Map_Name()).AsLower() + "_Skip";
	LoadGenericINISettings(ini, mapName, map, global);
}

void Skip::OnLoadLevel()
{
	if (map.Enabled)
	{
		Console_Input("msg Skip is enabled for this map Type !skip to cast your vote.");

		GameObject *o = Commands->Create_Object("invisible_object", Vector3(0, 0, 0));
		if (o)
		{
			Attach_Script_Once(o, "zbl_Skip_Controller", "");
		}
	}
}

void Skip::OnGameOver()
{
	SkipVoteController::Stop(false);
}

void Skip::OnPlayerLeave(int ID)
{
	// Cancel the player's vote if enabled.

	if (map.Enabled)
	{
		SkipVoteController::DeleteVote(ID);
	}
}

bool Skip::OnChat(int ID, TextMessageEnum Type, const wchar_t *Message, int recieverID)
{
	if (Message[0] == L'!')
	{
		if (map.Enabled)
		{
			if (wcsistr(Message, L"!skip") == Message)
			{
				SkipVoteController::CastVote(ID);
				return false;
			}

			if (wcsistr(Message, L"!cskip") == Message)
			{
				SkipVoteController::DeleteVote(ID);
				return false;
			}
		}
	}

	return true;
}



Skip plugin;
extern "C" __declspec(dllexport) Plugin* Plugin_Init()
{
	return &plugin;
}
