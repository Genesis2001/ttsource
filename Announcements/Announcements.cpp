/*	Renegade Scripts.dll
	Copyright 2013 Tiberian Technologies

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#include "General.h"
#include "Announcements.h"
#include "engine_tt.h"
#include "engine_io.h"
#include "gmgame.h"

#pragma warning (disable: 4996)

Announcements::Announcements()
{
	RegisterEvent(EVENT_GLOBAL_INI, this);
	RegisterEvent(EVENT_LOAD_LEVEL_HOOK, this);
}

Announcements::~Announcements()
{
	UnregisterEvent(EVENT_GLOBAL_INI, this);
	UnregisterEvent(EVENT_LOAD_LEVEL_HOOK, this);
}

void Announcements::OnLoadGlobalINISettings(INIClass *ini)
{
	config.Interval = ini->Get_Float("Announcements", "Interval", 420.0);
	
	int count = ini->Entry_Count("Announcements");
	for(int i = 0; i < count ; ++i)
	{
		const char *Entry = ini->Get_Entry("Announcements", i);

		// #pragma warning (disable: 4996)
		if (stricmp(Entry, "Interval") == 0)
		{
			// Apparently it doesn't like a -1 above @count. So we'll ignore it here.
			continue;
		}

		StringClass buf;
		ini->Get_String(buf, "Announcements", Entry);

		if (!buf)
		{
			break;
		}

		config.AnnouncementList.Add(buf);
	}
}

void Announcements::OnLoadLevel()
{
	GameObject *o = Commands->Create_Object("invisible_object", Vector3(0, 0, 0));
	Attach_Script_Once(o, "SSGM_AutoAnnouncement", "");
}

Announcements plugin;

extern "C" __declspec(dllexport) Plugin* Plugin_Init()
{
	return &plugin;
}
