#include "General.h"
#include "scripts.h"
#include "engine.h"
#include "zblpow.h"

void zbl_Change_Character_Powerup::Custom(GameObject *o, int type, int param, GameObject *sender)
{
	if (type == CUSTOM_EVENT_POWERUP_GRANTED)
	{
		Change_Character(sender, Get_Parameter("Preset"));
	}
}

void zbl_Powerup_Send_Custom::Custom(GameObject *o, int type, int param, GameObject *sender)
{
	if (type == CUSTOM_EVENT_POWERUP_GRANTED)
	{
		GameObject *obj = Commands->Find_Object(Get_Int_Parameter("ID"));
		if (obj)
		{
			int message = Get_Int_Parameter("Message");
			Commands->Send_Custom_Event(sender, obj, message, 0, 0);
		}
	}
}

void zbl_Powerup_Send_Custom_Team::Custom(GameObject *o, int type, int param, GameObject *sender)
{
	if (type == CUSTOM_EVENT_POWERUP_GRANTED)
	{
		if (!CheckPlayerType(sender, Get_Int_Parameter("TeamID")))
		{
			GameObject *obj = Commands->Find_Object(Get_Int_Parameter("ID"));
			if (obj)
			{
				int message = Get_Int_Parameter("Message");
				Commands->Send_Custom_Event(sender, obj, message, 0, 0);
			}
		}
	}
}

void zbl_Powerup_Send_Custom_Param::Custom(GameObject *o, int type, int param, GameObject *sender)
{
	if (type == CUSTOM_EVENT_POWERUP_GRANTED)
	{
		GameObject *obj = Commands->Find_Object(Get_Int_Parameter("ID"));
		if (obj)
		{
			int message = Get_Int_Parameter("Message");
			int parameter = Get_Int_Parameter("Parameter");
			Commands->Send_Custom_Event(sender, obj, message, parameter, 0);
		}
	}
}

//ScriptRegistrant<zbl_Change_Character_Powerup> zbl_Change_Character_Powerup_Registrant("zbl_Change_Character_Powerup", "Preset:string");
//ScriptRegistrant<zbl_Powerup_Send_Custom> zbl_Powerup_Send_Custom_Registrant("zbl_Powerup_Send_Custom", "ID:int,Message:int");
//ScriptRegistrant<zbl_Powerup_Send_Custom_Team> zbl_Powerup_Send_Custom_Team_Registrant("zbl_Powerup_Send_Custom_Team", "ID:int,Message:int,TeamID:int");
//ScriptRegistrant<zbl_Powerup_Send_Custom_Param> zbl_Powerup_Send_Custom_Param_Registrant("zbl_Powerup_Send_Custom_Param", "ID:int,Message:int,Parameter:int");
