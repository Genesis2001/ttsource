#include "General.h"

Player **Players = 0;

void PlayerClass::Startup()
{
	Players = (Player **)malloc(sizeof(Player)*128);
	memset((void *)Players, 0, sizeof(Player)*128);
}

void PlayerClass::Shutdown()
{
	if (Players)
	{
		for(int i = 0; i < 128; ++i)
		{
			if (Players[i])
			{
				free((void *)Players[i]);
			}
		}

		free((void *)Players);
		Players = 0;
	}
}

void PlayerClass::Joined(int ID)
{
	SList<cPlayer> *playerList = Get_Player_List();
	for(SLNode<cPlayer> *x = playerList->Head(); x != 0; x = x->Next())
	{
		cPlayer *tmp = (cPlayer *)x->Data();
		if (tmp)
		{
			GameObject *o = tmp->Get_GameObj();
			if (o && Get_Player_ID(o) == ID)
			{
				Player *p = new Player;
				p->ID = ID;
				p->data = tmp;

				p->AccessLevel = 0;

				const char *nick = Get_Player_Name_By_ID(ID);
				strcpy(p->Nick, nick);

				Players[ID] = p;
			}
		}
	}
}

void PlayerClass::MapLoad()
{
	SList<cPlayer> *playerList = Get_Player_List();
	for(SLNode<cPlayer> *x = playerList->Head(); x != 0; x = x->Next())
	{
		cPlayer *tmp = (cPlayer *)x->Data();
		if (tmp)
		{
			GameObject *o = tmp->Get_GameObj();
			if (o)
			{
				const char *nick = Get_Player_Name(o);
				Player *p = FindByName(nick);
				if (!p)
				{
					p = new Player;
					p->ID = Get_Player_ID(o);
					p->data = tmp;

					p->AccessLevel = 0;
					strcpy(p->Nick, nick);

					Players[p->ID] = p;
				}
				else 
				{
					Players[p->ID] = 0;

					Player *p_ = new Player;
					p_->ID = p->ID;
					p_->data = p->data;

					p_->AccessLevel = p->AccessLevel;

					strcpy(p_->Nick, p->Nick);

					Players[p_->ID] = p_;
					free((void *)p);
				}
			}
		}
	}
}

void PlayerClass::Leave(int ID)
{
	Player *p = Find(ID);
	if (p)
	{
		free((void *)p);
	}
}

Player *PlayerClass::Find(int ID)
{
	if (!Players)
	{
		return 0;
	}

	if (!Players[ID])
	{
		return 0;
	}

	return Players[ID];
}

Player *PlayerClass::FindByName(const char *name)
{
	if (!Players)
	{
		return 0;
	}

	for(int i = 0; i < 128; ++i)
	{
		if (!Players[i])
		{
			continue;
		}

		if (stricmp(Players[i]->Nick, name) == 0)
		{
			return Players[i];
		}
	}

	return 0;
}
