#include "General.h"
#include "Announcements.h"

class SSGM_AutoAnnouncement : public ScriptImpClass
{
public:
	void Created(GameObject *o)
	{
		Commands->Start_Timer(o, this, plugin.config.Interval, 444);
	}

	void Timer_Expired(GameObject *o, int num)
	{
		if (num == 444)
		{
			if (The_Game()->Get_Current_Players() > 0)
			{
				int rand = Commands->Get_Random_Int(0, plugin.config.AnnouncementList.Count() - 1);

				StringClass msg;
				msg.Format("msg %s", plugin.config.AnnouncementList[rand]);

				Console_Input(msg);
			}

			Commands->Start_Timer(o, this, plugin.config.Interval, 444);
		}
	}
};

ScriptRegistrant<SSGM_AutoAnnouncement> SSGM_AutoAnnouncement_Registrant("SSGM_AutoAnnouncement", "");
