#pragma once

class zbl_Change_Character_Powerup : public ScriptImpClass
{
public:
	void Custom(GameObject *o, int type, int param, GameObject *sender);
};

class zbl_Powerup_Send_Custom : public ScriptImpClass
{
public:
	void Custom(GameObject *o, int type, int param, GameObject *sender);
};

class zbl_Powerup_Send_Custom_Team : public ScriptImpClass
{
public:
	void Custom(GameObject *o, int type, int param, GameObject *sender);
};

class zbl_Powerup_Send_Custom_Param : public ScriptImpClass
{
public:
	void Custom(GameObject *o, int type, int param, GameObject *sender);
};
