/*	Renegade Scripts.dll
	Copyright 2013 Tiberian Technologies

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#include "General.h"
#include "Polls.h"
#include "engine_tt.h"
#include "engine_io.h"
#include "gmgame.h"
#include "Functions.h"
#include "zblscripts.h"

Polls::Polls()
{
	RegisterEvent(EVENT_GLOBAL_INI, this);
	RegisterEvent(EVENT_MAP_INI, this);
	RegisterEvent(EVENT_CHAT_HOOK, this);
	RegisterEvent(EVENT_LOAD_LEVEL_HOOK, this);
	RegisterEvent(EVENT_GAME_OVER_HOOK, this);
}

Polls::~Polls()
{
	UnregisterEvent(EVENT_GLOBAL_INI, this);
	UnregisterEvent(EVENT_MAP_INI, this);
	UnregisterEvent(EVENT_CHAT_HOOK, this);
	UnregisterEvent(EVENT_LOAD_LEVEL_HOOK, this);
	UnregisterEvent(EVENT_GAME_OVER_HOOK, this);
}

void Polls::OnLoadGlobalINISettings(INIClass *ini)
{
	global.SetDefaults();
	LoadGenericINISettings(ini, "Polls", global, global);
}

void Polls::OnLoadMapINISettings(INIClass *ini)
{
	StringClass mapName = The_Game()->Get_Map_Name();
	LoadGenericINISettings(ini, mapName, map, global);
}

void Polls::OnObjectCreate(void *data, GameObject *o)
{
}

void Polls::OnPlayerLeave(int ID)
{
	GameObject *o = Get_Controller();
	if (o)
	{
		Commands->Send_Custom_Event(o, o, POLL_CUSTOM_PLAYER_LEAVE, ID, 0.0f);

		if (The_Game()->Get_Current_Players() == 0)
		{
			Commands->Send_Custom_Event(o, o, POLL_CUSTOM_STOP, POLL_PARAM_NOPLAYERS, 0.0f);
		}
	}
}

void Polls::OnLoadLevel()
{
	GameObject *o = Commands->Create_Object("invisible_object", Vector3(0, 0, 0));
	Attach_Script_Once(o, "zbl_Poll_Controller", "");

	m_iController = Commands->Get_ID(o);
}

void Polls::OnGameOver()
{
}

bool Polls::OnChat(int ID, TextMessageEnum Type, const wchar_t *Message, int recieverID)
{
	if (Message[0] == L'!')
	{
		if (map.Enabled)
		{
			if (wcsistr(Message, L"!skip") == Message)
			{
				SkipVote(ID);
				return false;
			}
			else if (wcsistr(Message, L"!vote yes") == Message)
			{
				Vote(ID, POLL_PARAM_VOTE_YES);
				return true; // we want it to continue going through so others can use the same strings.
			}
			else if (wcsistr(Message, L"!vote no") == Message)
			{
				Vote(ID, POLL_PARAM_VOTE_NO);
				return true; // we want it to continue going through so others can use the same strings.
			}

			if (wcsistr(Message, L"!poll gameover") == Message && map.PollGameover)
			{
				ConsoleInputF("msg poll gameover not available right now.");
				return true;
			}

			char mapName[80];
			if (swscanf(Message, L"!poll setnext %s", &mapName) == 1 && map.PollSetNext)
			{
				ConsoleInputF("msg poll setnext not available right now. Debug param: %s", mapName);
				return true;
			}
		}
	}

	return true;
}

Polls plugin;
extern "C" __declspec(dllexport) Plugin* Plugin_Init()
{
	return &plugin;
}
