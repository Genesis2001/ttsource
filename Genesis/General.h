/*	Renegade Scripts.dll
	Copyright 2013 Tiberian Technologies

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/

#ifndef _INCLUDE__GENERAL_H
#define _INCLUDE__GENERAL_H

#include "Defines.h"
#include "Standard.h"

#include <windows.h>
#include <new>
#include "time.h"

#include "stdio.h"
#include "stdlib.h"

#ifdef SSGM
#include "scripts.h"
#include "engine.h"
#endif

#define Alloc(Type) new Type
#define TAlloc(Type, Size) (Type)malloc(Size)
#define CAlloc(Size) malloc(Size)
#define RAlloc(OldPtr, NewSize) realloc(OldPtr, NewSize)
#define Dealloc(Type, Ptr) free((Type *)Ptr)
#define CDealloc(Ptr) free((void *)Ptr)

#define M_PI 3.14159265358979323846

template <class T> class Stacker;
#define IterateStack(Var, Type, Stack) Type Var; \
	Stack->Reset(); \
	while(Stack->Iterate(&Var))

#pragma warning(disable: 4996 4345 4127)

#include "Settings.h"

template <class T> class Stacker
{
	T *Data;
	int Position;
	int Size;
public:
	Stacker()
	{

		Data = 0;
		Position = 0;
		Size = 0;
	}
	~Stacker()
	{
		if (Data)
		{
			CDealloc((void *)Data);
		}
	}

	int Push(const T Item)
	{
		if (!Data)
		{
			Data = (T *)CAlloc(sizeof(T)*1);
			Size = 1;
			Data[0] = Item;
			return 1;
		}
		Size++;
		Data = (T *)RAlloc((void *)Data, sizeof(T)*Size);
		Data[Size-1] = Item;
		return Size;
				
	}

	T Pop()
	{
		if (!Data || Size == 0)
		{
			return 0;
		}

		Size--;
		T item = Data[Size];
		Data = (T *)RAlloc((void *)Data, sizeof(T)*Size);
		return item;
	}

	bool Pop(T *Item)
	{
		if (!Data)
		{
			return 0;
		}

		Size--;
		(*Item) = Data[Size];
		Data = (T *)RAlloc((void *)Data, sizeof(T)*Size);
		return 1;
	}

	bool Iterate(T **Item)
	{
		if (!Data)
		{
			return 0;
		}
		if(Position >= Size)
		{
			return 0;
		}

		(*Item) = &Data[Position];
		Position++;
		return 1;
	}

	bool Iterate(T *Item)
	{
		if(!Data)
		{
			return 0;
		}
		if(Position >= Size)
		{
			return 0;
		}

		(*Item) = Data[Position];
		Position++;
		return 1;
	}

	T *Iterate()
	{
		if(!Data)
		{
			return 0;
		}
		if(Position >= Size)
		{
			return 0;
		}

		Position++;
		return &Data[Position-1];
	}

	void Reset()
	{
		Position = 0;
	}

	const T operator [](int Pos)
	{
		return Data[Pos];
	}

	const T *At(int Pos)
	{
		if(!Data)
		{
			return 0;
		}
		if(Pos >= Size)
		{
			return 0;
		}
		return &Data[Pos];
	}

	void Erase(int Pos)
	{
		if(Pos >= Size)
		{
			return;
		}		
		for(int i = Pos; i < Size; i++)
		{
			Data[i] = Data[i+1];
		}
		
		Size--;
		Data = (T *)RAlloc((void *)Data, sizeof(T)*Size);
	}

	void Insert(int Pos, T Item)
	{
		if((Pos >= Size) || !Data)
		{
			this->Push(Item);
			return;
		}

		if(Pos < 0)
		{
			this->Insert(0, Item);
			return;
		}

		Size++;
		Data = (T *)RAlloc((void *)Data, sizeof(T)*Size);
		for(int i = Size; i >= Pos; i--)
		{
			Data[i] = Data[i-1];
		}
		Data[Pos] = Item;
	}

	void Clear()
	{
		if(Data)
		{
			CDealloc((void *)Data);
		}
		Data = 0;
		Position = 0;
		Size = 0;
	}
	
	const int Length()
	{
		return Size;
	}

	bool Empty()
	{
		if(Data)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}

	void Edit(int At, T &item)
	{
		if(!Data)
		{
			return;
		}
		
		if(At >= Size)
		{
			return;
		}

		Data[At] = item;
	}
};

struct Player
{
public:
	int ID;
	char Nick[45];
	
	cPlayer *data;

	int AccessLevel;

	float Experience;
};

#include "PlayerClass.h"
#include "sqlite3.h"
#include "zbldb.h"

#endif
