//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by tdbedit.rc
//
#define IDSOUND                         3
#define IDR_MAIN                        101
#define IDD_Goto                        102
#define IDD_Find_ID                     103
#define IDD_Find_String                 104
#define IDD_Find_Eng_String             105
#define IDD_Find_Results                106
#define IDD_Info                        107
#define IDD_New_Category                108
#define IDI_MAIN                        110
#define IDD_SOUNDLIST                   111
#define IDC_TEXT                        1001
#define IDC_GOTO_INT                    1004
#define IDC_FIND_STRING                 1005
#define IDC_OK                          1006
#define IDC_CANCEL                      1007
#define IDC_CLEAR                       1009
#define IDC_CLOSE                       1010
#define IDC_Results                     1011
#define IDC_ID                          1012
#define IDC_ANIMATION                   1013
#define IDC_RADIOIDX                    1013
#define IDC_SOUND_ID                    1014
#define IDC_RADIOID                     1014
#define IDC_ENG_STRING                  1015
#define IDC_CATEGORY                    1015
#define IDC_ID_N                        1016
#define IDC_TREE                        1183
#define ID_NEW                          40020
#define ID_OPEN                         40021
#define ID_SAVE                         40023
#define ID_SAVEAS                       40024
#define ID_EXIT                         40025
#define ID_GOTO                         40026
#define ID_FIND_BY_ENG_STRING           40029
#define ID_FIND_BY_STRING               40030
#define ID_FIND_BY_ID                   40031
#define ID_CLOSE_FILE                   40038
#define ID_FILE_EXPORTCHEADER           40044
#define ID_EXPORT                       40045
#define ID_EXPORT_HEADER                40046
#define ID_EDIT_ADDSTRING               40047
#define ID_EDIT_REMOVESTRING            40048
#define ID_EDIT_ADDCATEGORY             40049
#define ID_EDIT_REMOVECATEGORY          40050
#define ID_ADDSTRING                    40051
#define ID_REMOVESTRING                 40052
#define ID_ADDCATEGORY                  40053
#define ID_REMOVECATEGORY               40054
#define ID_EXPORT_TABLE                 40055
#define ID_IMPORT_TABLE                 40056
#define ID_FILE_OPENDDB                 40057
#define ID_OPEN_DDB                     40058
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        112
#define _APS_NEXT_COMMAND_VALUE         40059
#define _APS_NEXT_CONTROL_VALUE         1016
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
