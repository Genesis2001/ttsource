#include "General.h"
#include "engine.h"
#include "scripts.h"
#include "Skip.h"

class zbl_Skip_Player : public ScriptImpClass
{
public:
	void Created(GameObject *o)
	{
		Commands->Attach_Script(o, "zbl_Skip_Key", "");
		Commands->Attach_Script(o, "zbl_CSkip_Key", "");
	}
};

class zbl_Skip_Key : public JFW_Key_Hook_Base
{
public:
	void Created(GameObject *o)
	{
		InstallHook("SkipKey", o);
	}

	void KeyHook()
	{
		Skip_Cast_Vote(Owner());
	}
};

class zbl_CSkip_Key : public JFW_Key_Hook_Base
{
public:
	void Created(GameObject *o)
	{
		InstallHook("CSkipKey", o);
	}

	void KeyHook()
	{
		Skip_Delete_Vote(Owner());
	}
};

ScriptRegistrant<zbl_Skip_Player> zbl_Skip_Player_Registrant("zbl_Skip_Player", "");
ScriptRegistrant<zbl_Skip_Key> zbl_Skip_Key_Registrant("zbl_Skip_Key", "");
ScriptRegistrant<zbl_CSkip_Key> zbl_CSkip_Key_Registrant("zbl_CSkip_Key", "");
