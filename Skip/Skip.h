/*	Renegade Scripts.dll
	Copyright 2013 Tiberian Technologies

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/

#pragma once

#include "gmplugin.h"

void PPage(int ID, const char *Msg, ...);
void PPageErr(int ID, const char *Msg, ...);

void Skip_Cast_Vote(GameObject *plr);
void Skip_Delete_Vote(GameObject *plr);

class Skip : public Plugin
{
public:
	struct Configuration
	{
		bool Enabled;

		float Threshold;
		int Count;

		void SetDefaults()
		{
			Enabled = true;
			Threshold = 120;
			Count = 51;
		}
	};

	Configuration global;
	Configuration map;

	Skip();
	~Skip();
	
	void OnLoadGlobalINISettings(INIClass *ini);
	void OnLoadMapINISettings(INIClass *ini);
	void OnPlayerLeave(int ID);
	void OnLoadLevel();
	void OnGameOver();
	bool OnChat(int PlayerID, TextMessageEnum Type, const wchar_t *Message, int recieverID);

private:
	void LoadGenericINISettings(INIClass *ini, const char *section, Configuration &config, const Configuration &def)
	{
		config.Enabled		= ini->Get_Bool(section, "Enabled", def.Enabled);
		config.Count		= ini->Get_Int(section, "Count", def.Count);
		config.Threshold	= ini->Get_Float(section, "Threshold", def.Threshold);
	}
};

class SkipVoteController
{
	static int m_iSkipCount;
	static bool m_bSkipStarted;
	static DynamicVectorClass<int> m_lPlayers;
	
public:
	static void CastVote(int ID)
	{
		if (!m_bSkipStarted)
		{
			Start();

			StringClass msg;
			msg.Format("msg %s has started a vote to skip the current map. Type !skip to cast your vote.", Get_Player_Name_By_ID(ID));
			Console_Input(msg);
		}

		if (Has_Player_Voted(ID))
		{
			PPage(ID, "Your skip vote has already been counted.");
			return;
		}

		m_iSkipCount++;
		m_lPlayers.Add(ID);

		PPage(ID, "Your vote has been counted.");
	}

	static const int Count()
	{
		return m_iSkipCount;
	}

	static void DeleteVote(int ID)
	{
		if (Has_Player_Voted(ID))
		{
			m_iSkipCount--;
			m_lPlayers.Delete(ID);
			PPage(ID, "Your skip vote has been cancelled.");
		}
	}

	static bool Has_Player_Voted(int ID)
	{
		for(int i = 0; i < m_lPlayers.Count(); ++i)
		{
			if (i == ID)
			{
				return true;
			}
		}

		return false;
	}

	static void Start()
	{
		if (m_bSkipStarted) return;


	}

	static void Stop(bool processVotes = true)
	{
	}
};
