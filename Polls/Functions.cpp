#include "General.h"
#include "engine.h"
#include "scripts.h"
#include "Functions.h"

void PPage(int ID, const char *Msg, ...)
{
	char buffer[256];
	va_list va;
	_crt_va_start(va, Msg);
	vsnprintf(buffer, 256, Msg, va);
	va_end(va);

	GameObject *plr = Get_GameObj(ID);
	if (plr)
	{
		Send_Message_Player(plr, 15, 246, 96, buffer);
		Create_2D_Sound_Player(plr, "yo1.wav");
	}
}

void PPageErr(int ID, const char *Msg, ...)
{
	char buffer[256];
	va_list va;
	_crt_va_start(va, Msg);
	vsnprintf(buffer, 256, Msg, va);
	va_end(va);

	GameObject *plr = Get_GameObj(ID);
	if (plr)
	{
		Send_Message_Player(plr, 255, 107, 107, buffer);
		Create_2D_Sound_Player(plr, "yo1.wav");
	}
}

void ConsoleInputF(const char *Format, ...)
{
	char buffer[256];
	va_list va;
	_crt_va_start(va, Format);
	vsnprintf(buffer, 256, Format, va);
	va_end(va);

	Console_Input(buffer);
}
