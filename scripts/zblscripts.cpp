#include "General.h"
#include "zblscripts.h"
#include "zbl.h"

class zbl_MXX_Change_Character_PowerUp : public ScriptImpClass { };
class zbl_MXX_Change_Character_PowerUp_Team : public ScriptImpClass { };
class zbl_MXX_Class_Powerup : public ScriptImpClass { };

class zbl_M01_Clear_Weapons_Created : public ScriptImpClass { };
class zbl_M01_Enable_Hud_Created : public ScriptImpClass { };
class zbl_M01_Enable_Hud_Custom : public ScriptImpClass { };
class zbl_Refill_Buy_Poke : public ScriptImpClass { };
class zbl_Limit_Preset : public ScriptImpClass { };
class zbl_Poke_Buy_Repair_Object : public ScriptImpClass { };
class zbl_Poke_Buy_Repair_Object_Send_Custom : public ScriptImpClass { };
class zbl_Zone_Enter_Send_Custom_Object : public ScriptImpClass { };

Register_Script(zbl_MXX_Change_Character_PowerUp, "Preset=Nod_Turret_Destroyed:string");
Register_Script(zbl_MXX_Change_Character_PowerUp_Team, "NodPreset:string,GDIPreset:string");
Register_Script(zbl_MXX_Class_Powerup, "Team=1:int,MaxUnits=1:int,Preset=CnC_GDI_Blah:string,Clear_Weapons=0:int");
Register_Script(zbl_M01_Clear_Weapons_Created, "");
Register_Script(zbl_M01_Enable_Hud_Custom, "Message:int,Enable=1:int");
Register_Script(zbl_M01_Enable_Hud_Created, "Enable=1:int");

Register_Script(zbl_Limit_Preset, "Team:int,Max:int");
Register_Script(zbl_Refill_Buy_Poke, "Cost:int,Player_Type:int,Buy_Sound:string,Denied_Sound:string,Insufficient_Funds_Sound:string,Ammo_PowerUp:string");

Register_Script(zbl_Poke_Buy_Repair_Object, "Player_Type:int,Cost:int,ObjectID:int,Amount:float,Cooldown_Timer:float,Buy_Sound:string,Denied_Sound:string,NoMoney_Sound:string,Cooldown_Denied_Sound:string");
Register_Script(zbl_Poke_Buy_Repair_Object_Send_Custom, "Player_Type:int,Cost:int,ObjectID:int,Amount:float,Cooldown_Timer:float,Buy_Sound:string,Denied_Sound:string,NoMoney_Sound:string,Cooldown_Denied_Sound:string,Send_Object:int,Succeeded_Message:int,Denied_Message:int,NoMoney_message:int,Cooldown_Message:int");
Register_Script(zbl_Zone_Enter_Send_Custom_Object, "Player_Type:int,ID:int,Message:int,Param:int");
