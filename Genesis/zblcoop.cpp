#include "General.h"
#include "scripts.h"
#include "engine.h"
#include "zblcoop.h"

zbl_Coop_Controller::zbl_Coop_Controller()
{
}

void zbl_Coop_Controller::Created(GameObject *o)
{
	m_iMaxSupport = Get_Int_Parameter("MaxSupport");
	m_iMaxTechie = Get_Int_Parameter("MaxTechie");
	m_iMaxRecon = Get_Int_Parameter("MaxRecon");
	m_iMaxHeavyWeapons = Get_Int_Parameter("MaxHeavyWeapons");
}

void zbl_Coop_Controller::Custom(GameObject *o, int type, int param, GameObject *sender)
{
	if (type == ZBL_COOP_CUSTOM_CLASS_REGISTER)
	{

	}
}


void zbl_Coop_Player::Created(GameObject *o)
{
}

void zbl_Coop_Player::Custom(GameObject *o, int type, int param, GameObject *sender)
{
}

void zbl_Coop_Change_Class::Created(GameObject *o)
{
	if (!o->As_SoldierGameObj() || !Get_Player_ID(o))
	{
		Destroy_Script();
	}

	GameObject *controller = Find_Object_With_Script("zbl_Coop_Controller");
	if (controller)
	{
		// TODO: Send custom event to controller object registering this class as used.

		// Parameters: ClassID:int, 
	}
}


//ScriptRegistrant<zbl_Coop_Controller> zbl_Coop_Controller_Registrant("zbl_Coop_Controller", "Default_Soldier_Preset=CnC_GDI_MiniGunner_0:string,MaxHeavyWeapons=1:int,MaxSupport=1:int,MaxTechie=1:int,MaxRecon=1:int");
//ScriptRegistrant<zbl_Coop_Player> zbl_Coop_Player_Registrant("zbl_Coop_Player", "");
//ScriptRegistrant<zbl_Coop_Change_Class> zbl_Coop_Change_Class_Registrant("zbl_Coop_Change_Class", "");
