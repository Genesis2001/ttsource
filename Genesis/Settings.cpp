#include "General.h"

void Settings::Load()
{
	char buffer[128];
	GetCurrentDirectory(64, buffer);
	strcat(buffer, "\\Config\\Genesis.ini");

	INIClass *ini = Get_INI(buffer);
	if (!ini)
	{
		printf("[Genesis] Unable to read the configuration file.\n");
		
		ini = NULL;
		return;
	}

	LoadGenericSettings(ini, "Genesis", Global);

	Release_INI(ini);
}

void Settings::MapLoad(const char *map)
{
	char buffer[128];
	GetCurrentDirectory(64, buffer);
	strcat(buffer, "\\Config\\Genesis.ini");

	INIClass *ini = Get_INI(buffer);
	if (!ini)
	{
		printf("[Genesis] Unable to read the configuration file.\n");
		
		ini = NULL;
		return;
	}

	LoadGenericSettings(ini, map, Map, Global);

	Release_INI(ini);
}

void Settings::Unload()
{
}

void Settings::LoadGenericSettings(INIClass *ini, const char *section, Configuration &config, const Configuration &def)
{
	config.GameMode = ini->Get_Int(section, "GameMode", def.GameMode);
}

Settings::Configuration Settings::Global;
Settings::Configuration Settings::Map;
