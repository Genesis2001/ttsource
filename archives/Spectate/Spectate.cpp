/*	Renegade Scripts.dll
	Copyright 2013 Tiberian Technologies

	This file is part of the Renegade scripts.dll
	The Renegade scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/
#include "General.h"
#include "Spectate.h"
#include "gmgame.h"
#include "Iterator.h"
#include "CommandLineParser.h"
#include "engine_tt.h"
#include "SoldierGameObj.h"
#include "VehicleGameObj.h"
#include "WeaponBagClass.h"
#include "engine_weap.h"
#include "engine_obj2.h"

static int getClientIdByIdentifier(const char* clientIdentifier)
{
	TT_ASSERT(clientIdentifier);
	const cPlayer* player = Find_Player(atoi(clientIdentifier));
	int result;
	if (player)
		result = player->Get_Id();
	else
		result = -1;
	return result;
}

static bool isClientId(const int id)
{
	return id > 0 && id < 128 && Find_Player(id);
}


class CommandSPECTATE :
	public ConsoleFunctionClass
{
public:
	const char* Get_Name() 
	{ 
		return "Spectate"; 
	}
	const char* Get_Help() 
	{ 
		return "SPECTATE <clientId> - Allows a client to enter spectate mode."; 
	}
	void Activate(const char* argumentsString)
	{
		CommandLineParser arguments(argumentsString);
		const char* clientIdentifier = arguments.getString();

		if (!clientIdentifier || clientIdentifier[0] == '\0')
			Console_Output("Please enter a client identifier.");
		else
		{
			const int clientId = getClientIdByIdentifier(clientIdentifier);
			if (!isClientId(clientId))
			{
				Console_Output("Please enter a valid client identifier.");
			}
			else
			{
				Spectate(Get_GameObj(clientId),clientId);
			}
		}
	}
};

class CommandSPECTATESPEED :
	public ConsoleFunctionClass
{
public:
	const char* Get_Name() 
	{ 
		return "SpectateSpeed"; 
	}
	const char* Get_Help() 
	{ 
		return "SPECTATESPEED <clientId> <speed> - Sets the max speed of a spectating player"; 
	}
	void Activate(const char* argumentsString)
	{
		CommandLineParser arguments(argumentsString);
		const char* clientIdentifier = arguments.getString();

		if (!clientIdentifier || clientIdentifier[0] == '\0')
			Console_Output("Please enter a client identifier.");
		else
		{
			const int clientId = getClientIdByIdentifier(clientIdentifier);
			if (!isClientId(clientId))
			{
				Console_Output("Please enter a valid client identifier.");
			}
			else
			{
				double speed = arguments.getDouble();
				GameObject *obj = Get_GameObj(clientId);
				int team = Get_Team(Get_Player_ID(obj));
				if(team == -4)
				{
					obj->As_SoldierGameObj()->Set_Max_Speed((float)speed);
				}
			}
		}
	}
};

class SPECTATE : public Plugin
{
public:
	SPECTATE()
	{
		ConsoleFunctionList.Add(new CommandSPECTATE);
		ConsoleFunctionList.Add(new CommandSPECTATESPEED);
		Sort_Function_List();
		Verbose_Help_File();
		Console_Output("Loading Spectate Plugin; written by Reborn from MP-Gaming.COM\n");
	}
	~SPECTATE()
	{
		Console_Output("Un-loading Spectate Plugin; written by Reborn from MP-Gaming.COM\n");
		Delete_Console_Function("Spectate");
		Delete_Console_Function("SpectateSpeed");
	}
	
};

SPECTATE spectate;
SCRIPTS_API uint Send_Object_Update(NetworkObjectClass* object, int remoteHostId);
void Spectate(GameObject *obj,int id)
{
	SoldierGameObj *soldier = obj->As_SoldierGameObj();
	int team = soldier->Get_Player_Type();
	if(team != -4)
	{
		if (soldier->Get_Vehicle())
		{
			Console_Output("Can't enable spectate mode when in a vehicle\n");
			return;
		}
		if (!Get_Fly_Mode(soldier))
		{
			Toggle_Fly_Mode(soldier);
		}
		soldier->Set_Player_Type(-4);
		Commands->Set_Model(soldier,"NULL");
		Commands->Clear_Weapons(soldier);
		Commands->Disable_All_Collisions(soldier);
		Commands->Set_Is_Visible(soldier,false);
		Commands->Set_Is_Rendered(soldier,false);
		Commands->Set_Shield_Type(soldier,"Blamo");
		Disarm_All_C4_Beacons(id);
		for (SLNode<cPlayer>* PlayerIter = Get_Player_List()->Head(); (PlayerIter != NULL); PlayerIter = PlayerIter->Next())
		{
			cPlayer *p = PlayerIter->Data();
			if (!p->Is_Active()) continue;
			if (p->Get_Id() == id)
			{
				Send_Object_Update(obj,p->Get_Id());
			}
			else
			{
				bool pending = obj->Is_Delete_Pending();
				obj->Set_Is_Delete_Pending(true);
				Send_Object_Update(obj,p->Get_Id());
				obj->Set_Is_Delete_Pending(pending);
			}
		}
		soldier->Clear_Object_Dirty_Bits();
	}
	else
	{
		obj->Set_Delete_Pending();
	}
}


extern "C" __declspec(dllexport) Plugin* Plugin_Init()
{
	return &spectate;
}
