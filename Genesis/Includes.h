#pragma once

#include "General.h"
#include <windows.h>
#include "scripts.h"
#include "engine.h"

struct Player
{
public:
	int ID;
	char Nick[45];
	
	cPlayer *data;

	int AccessLevel;

	float Experience;
};

#include "PlayerClass.h"
