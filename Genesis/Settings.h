#pragma once

class Settings
{
public:
	struct Configuration
	{
		int GameMode;

		StringClass DatabaseFile;
		
		Configuration()
		{
			GameMode = 1;

			DatabaseFile = "Config\\Genesis.db";
		}
	};
	
	static void Load();
	static void MapLoad(const char *map);
	static void Unload();

	static Configuration Global;
	static Configuration Map;

private:
	static void LoadGenericSettings(INIClass *ini, const char *key, Configuration &config, const Configuration &def = Configuration());
};
