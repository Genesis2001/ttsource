#include "General.h"
#include "engine.h"
#include "scripts.h"
#include "zblscripts.h"
#include "Skip.h"

class zbl_Skip_Controller : public ScriptImpClass
{
	float skipEndTime;

public:
	void Timer_Expired(GameObject *o, int num)
	{
		if (num == SKIP_TIMER_END)
		{
			SkipVoteController::Stop();
			Destroy_Script();
		}
		else if (num == SKIP_TIMER_LOOP)
		{

		}
	}

	void Custom(GameObject *o, int type, int param, GameObject *sender)
	{
		if (type == SKIP_CUSTOM_START)
		{
			skipEndTime = (float)param;
			
			Commands->Start_Timer(o, this, skipEndTime, SKIP_TIMER_END);
			Commands->Start_Timer(o, this, skipEndTime / 4, SKIP_TIMER_LOOP);
		}
	}
};

ScriptRegistrant<zbl_Skip_Controller> zbl_Skip_Controller_Registrant("zbl_Skip_Controller", "");
