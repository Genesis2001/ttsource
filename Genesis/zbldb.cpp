#include "General.h"

sqlite3 *Database::db;

void sin_glue(sqlite3_context *ctx, int argc, sqlite3_value **argv)
{
	double rad = sqlite3_value_double(argv[0]);
	if(rad >= 360 || rad <= -360)
	{
		char err[64];
		strcpy(err, "sin: value out of range");
		sqlite3_result_error(ctx, err, strlen(err));
		return;
	}

	rad = rad * (M_PI / 180);
	rad = sin(rad);
	sqlite3_result_double(ctx, rad);
}

void cos_glue(sqlite3_context *ctx, int argc, sqlite3_value **argv)
{
	double rad = sqlite3_value_double(argv[0]);
	if(rad >= 360 || rad <= -360)
	{
		char err[64];
		strcpy(err, "sin: value out of range");
		sqlite3_result_error(ctx, err, strlen(err));
		return;
	}

	rad = rad * (M_PI / 180);
	rad = cos(rad);
	sqlite3_result_double(ctx, rad);
}

void tan_glue(sqlite3_context *ctx, int argc, sqlite3_value **argv)
{
	double rad = sqlite3_value_double(argv[0]);
	if(rad >= 360 || rad <= -360)
	{
		char err[64];
		strcpy(err, "sin: value out of range");
		sqlite3_result_error(ctx, err, strlen(err));
		return;
	}

	rad = rad * (M_PI / 180);
	rad = tan(rad);
	sqlite3_result_double(ctx, rad);
}

void Database::Startup(StringClass dbfile)
{
	if (sqlite3_open_v2(dbfile, &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL) != 0)
	{
		return;
	}

	sqlite3_create_function(db, "sin", 1, SQLITE_ANY, 0, sin_glue, 0, 0);
	sqlite3_create_function(db, "cos", 1, SQLITE_ANY, 0, cos_glue, 0, 0);
	sqlite3_create_function(db, "tan", 1, SQLITE_ANY, 0, tan_glue, 0, 0);
}

void Database::Shutdown()
{
	if (db)
	{
		sqlite3_close(db);
	}
}

const char *Database::Query(Stacker<Row *> *result, const char *Format, ...)
{
	va_list va;
	_crt_va_start(va, Format);
	char *buffer = sqlite3_vmprintf(Format, va);
	va_end(va);

	char *Error = 0;
	if (sqlite3_exec(db, buffer, Callback, (void *)result, &Error) != 0)
	{
		char *ret = strdup(Error);
		printf("Sqlite error: %s\n", Error);
		sqlite3_free(Error);
		sqlite3_free(buffer);

		if (result)
		{
			DeleteList(result);
		}
		return ret;
	}

	sqlite3_free(buffer);
	return 0;
}

void Database::DeleteResult(Stacker<Row *> *result)
{
	if (!result)
	{
		return;
	}

	if (result->Empty())
	{
		return;
	}

	Row *r;
	result->Reset();
	while(result->Iterate(&r))
	{
		Column *c;
		r->Columns->Reset();
		while(r->Columns->Iterate(&c))
		{
			CDealloc(c->Name);
			CDealloc(c->Data);
		}
		
		DeleteList(r->Columns);
		Dealloc(Stacker<Column *>, r->Columns);
	}

	DeleteList(result);
	result->Clear();
}

char *Database::GetColumnData(const char *name, Stacker<Column *> *row)
{
	Column *c;
	row->Reset();
	while(row->Iterate(&c))
	{
		if (!c)
		{
			break;
		}

		if (strcmp(c->Name, name) == 0)
		{	
			return c->Data;
		}
	}
	return "null";
}


int Database::Callback(void *data, int argc, char **ColumnData, char **Columns)
{
	Stacker<Row *> *l = (Stacker<Row *> *)data;
	if(!l)
	{
		return 0;
	}

	Row *r = Alloc(Row);
	r->Columns = Alloc(Stacker<Column *>);
	for(int i = 0; i < argc; i++)
	{
		Column *c = Alloc(Column);
		
		c->Name = strdup(Columns[i]);
		c->Data = strdup(ColumnData[i] ? ColumnData[i] : "NULL");
		r->Columns->Push(c);
	}

	l->Push(r);
	return 0;
}

void Database::DeleteList(void *stack)
{
	Stacker<void *> *s = (Stacker<void *> *)stack;

	void *it;
	s->Reset();
	while(s->Iterate(&it))
	{
		CDealloc(it);
	}

	s->Clear();
}
void Database::CreateTables()
{
	// CREATE TABLE Players(Uid INTERGER PRIMARY KEY, ID INTERGER, Nick char(32), JOINMSG char(256));
}