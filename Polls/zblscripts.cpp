#include "General.h"
#include "engine.h"
#include "scripts.h"
#include "Polls.h"
#include "Functions.h"
#include "zblscripts.h"

void Vote(int ID, int vote)
{
	GameObject *plr = Get_GameObj(ID);
	GameObject *controller = plugin.Get_Controller();

	if (plr && controller)
	{
		Commands->Send_Custom_Event(plr, controller, vote, 0, 0.0f);
	}
}
void SkipVote(int ID)
{
	GameObject *plr = Get_GameObj(ID);
	GameObject *controller = plugin.Get_Controller();

	if (plr && controller)
	{
		Commands->Send_Custom_Event(plr, controller, POLL_CUSTOM_VOTE, POLL_PARAM_VOTE_SKIP, 0.0f);
	}
}

const char *zbl_Poll_Controller::GetPollType()
{
	StringClass buf;
	if (m_iPollType == POLL_TYPE_SKIP)
	{
		 buf = "skip the current map";
	}
	else if (m_iPollType == POLL_TYPE_SETNEXT)
	{
		buf.Format("set the next map to %s", "(insert map name here)");
	}
	else if (m_iPollType == POLL_TYPE_GAMEOVER)
	{
		buf.Format("end the current game.");
	}
	else
	{
		buf = "ERR";
	}

	return buf;
}

void zbl_Poll_Controller::AddVote(int ID, int vote)
{
	if (m_iPollType == 0)
	{
		PPageErr(ID, "Error: no poll in progress.");
		return;
	}

	if (!m_bAcceptVotes)
	{
		PPageErr(ID, "Error: the poll has closed.");
		return;
	}

	if (vote == POLL_PARAM_VOTE_YES)
	{
		if (HasVoted(ID) && m_iPollType == POLL_TYPE_SKIP)
		{
			PPageErr(ID, "You have already voted to skip the map. Type !cskip to cancel your vote.");
			return;
		}

		// else if hasvoted && players[ID] == POLL_PARAM_VOTE_NO
		//		no--;
		//		yes++;
		//		PPage(ID, "Your vote has been changed to yes.");

		// else if 
	}
	else if (vote == POLL_PARAM_VOTE_NO)
	{

	}
}
bool zbl_Poll_Controller::HasVoted(int ID)
{
	std::map<int, int>::iterator it;
	for (it = m_lPlayers.begin(); it != m_lPlayers.end(); it++)
	{
		if (it->first == ID)
		{
			return true;
		}
	}

	return false;
}

void zbl_Poll_Controller::Created(GameObject *o)
{
	m_iPollType = 0;
	m_bAcceptVotes = false;

	m_iYesVotes = 0;
	m_iNoVotes = 0;
}

void zbl_Poll_Controller::Custom(GameObject *o, int type, int param, GameObject *sender)
{
	if (type == POLL_CUSTOM_START)
	{
		m_iPollType = param;

		const char *buf = GetPollType();
		if (strcmp(buf, "ERR") != 0)
		{
			ConsoleInputF("msg A poll has been started to %s. Cast your vote vote with !vote yes or !vote no.", buf);

			Commands->Start_Timer(o, this, plugin.map.PollTimer, POLL_TIMER_LOOP);
		}

		m_bAcceptVotes = true;

		delete[] buf;
	}
	else if (type == POLL_CUSTOM_STOP)
	{
		StringClass msg1, msg2;
		
		if (m_iPollType == POLL_TYPE_SKIP)
		{
			int skips = m_iYesVotes;
			int total = The_Game()->Get_Current_Players();
			
			if (total == 0)
			{ // divide by zero...
				total = 1;
			}

			int percent = skips / total;



			msg1.Format("msg Results: %d/%d (%.0d%%) wanted to skip this map. %s", skips, total, percent);
		}

		if (msg1.Get_Length() > 0)
		{
			Console_Input(msg1);
		}

		if (msg2.Get_Length() > 0)
		{
			Console_Input(msg2);
		}

		// Host: [BR] Voting period over, tallying votes...   GDI; 4 Yes, 0 No Nod; 2 Yes, 2 No
		// Host: [BR] More yes votes than no votes. VOTE PASSED.
		// Host: [BR] Setting next map to Lake_Garden.
	}
	else if (type == POLL_CUSTOM_VOTE && param == POLL_PARAM_VOTE_SKIP && m_iPollType == 0)
	{
		Commands->Send_Custom_Event(sender, o, POLL_CUSTOM_START, POLL_TYPE_SKIP, 0.0f);
		Commands->Send_Custom_Event(sender, o, POLL_CUSTOM_VOTE, POLL_PARAM_VOTE_SKIP, 1.0f);
	}
	else if (type == POLL_CUSTOM_VOTE)
	{
		if (param == POLL_PARAM_VOTE_YES || param == POLL_PARAM_VOTE_SKIP)
		{
			m_iYesVotes++;
		}
		else if (param == POLL_PARAM_VOTE_SKIP_CANCEL)
		{
			m_iYesVotes--;
		}
		else if (param == POLL_PARAM_VOTE_NO)
		{
			m_iNoVotes++;
		}
	}
	else if (type == POLL_CUSTOM_VOTE && !m_bAcceptVotes)
	{
		int ID = Get_Player_ID(sender);
		PPageErr(ID, "No poll in progress.");
	}
}

void zbl_Poll_Controller::Timer_Expired(GameObject *o, int num)
{
	if (num == POLL_TIMER_LOOP)
	{
		if (m_iPollType == POLL_TYPE_SKIP)
		{
		}
		else
		{
			if (m_iLoopCount >= 1)
			{
				m_bAcceptVotes = false;
				ConsoleInputF("msg Polls closed, tallying votes...");

				m_iLoopCount = 0;
				Commands->Send_Custom_Event(o, o, POLL_CUSTOM_STOP, 1, 0.0f);
			}
			else
			{
				// Host: [BR] unknown18898 has initiated a vote to change the next map to Lake_Garden. You have 120 seconds to vote using !poll yes or !poll no.
				// Host: [BR] A vote is in progress to  change the next map to Lake_Garden. You have 60 seconds left to vote using !poll yes and !poll no.
				float time = plugin.map.PollTimer / 2;

				ConsoleInputF("msg A vote is in progress to %s");

				Commands->Start_Timer(o, this, time, POLL_TIMER_LOOP);
			}
		}
	}
}

ScriptRegistrant<zbl_Poll_Controller> zbl_Poll_Controller_Registrant("zbl_Poll_Controller", "");

