#pragma once

enum
{
	ZBL_COOP_CLASS_HEAVYWEAPONS = 1,
	ZBL_COOP_CLASS_SUPPORT,
	ZBL_COOP_CLASS_TECHIE,
	ZBL_COOP_CLASS_RECON,

	ZBL_COOP_CUSTOM_CLASS_REGISTER = 555
};

class zbl_Coop_Controller : public ScriptImpClass
{
	int m_iMaxSupport, m_iCurrentSupport;
	int m_iMaxTechie, m_iCurrentTechie;
	int m_iMaxRecon, m_iCurrentRecon;
	int m_iMaxHeavyWeapons, m_iCurrentHeavyWeapons;
	
public:
	zbl_Coop_Controller();

	void Created(GameObject *obj);
	void Custom(GameObject *o, int type, int param, GameObject *sender);
};

class zbl_Coop_Player : public ScriptImpClass
{
public:
	void Created(GameObject *o);
	void Custom(GameObject *o, int type, int param, GameObject *sender);
};

class zbl_Coop_Change_Class : public ScriptImpClass
{
public:
	void Created(GameObject *o);
};
