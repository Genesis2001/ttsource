#pragma once

#define Register_Script(ClassName,Params) ScriptRegistrant<ClassName> ClassName##_Registrant(#ClassName,Params)