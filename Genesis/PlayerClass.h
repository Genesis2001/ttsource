#pragma once

class PlayerClass
{
public:

	static void Startup();
	static void Shutdown();

	static void Joined(int ID);
	static void Leave(int ID);

	static void MapLoad();

	static Player *Find(int ID);
	static Player *FindByName(const char *name);
};

extern Player **Players;
